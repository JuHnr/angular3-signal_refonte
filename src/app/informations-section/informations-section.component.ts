import { Component, Input } from '@angular/core';
import { ButtonComponent } from '../button/button.component';

@Component({
  selector: 'app-informations-section',
  standalone: true,
  imports: [ButtonComponent],
  templateUrl: './informations-section.component.html',
  styleUrl: './informations-section.component.scss'
})
export class InformationsSectionComponent {

  @Input() title: string | undefined;
  @Input() description: string | undefined;
  @Input() pathImage: string | undefined;
  @Input() buttonContent: string | undefined;

}
